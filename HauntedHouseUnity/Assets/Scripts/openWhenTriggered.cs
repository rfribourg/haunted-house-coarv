using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class openWhenTriggered : MonoBehaviour
{
    public bool activated;
    private float angleFromBegin;
    public float rotationSpeed = -0.5f;
    // Start is called before the first frame update
    void Start()
    {
        activated = false;
        angleFromBegin = 0.0f;
    }

    // Update is called once per frame
    void Update()
    {
        if (activated && angleFromBegin > -90) // the target position is at -90 degrees 
        {
            angleFromBegin += rotationSpeed;
            this.transform.RotateAround(this.transform.position, this.transform.forward, rotationSpeed); // rotate the shed door around the vertical axis
        }
    }
}
