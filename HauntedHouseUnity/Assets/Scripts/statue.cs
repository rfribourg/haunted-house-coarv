using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class statue : MonoBehaviour
{
    public GameObject statue1;
    public GameObject statue2;
    public GameObject statue3;
    public GameObject statue4;
    public GameObject statue5;

    public GameObject statue_part1;
    public GameObject statue_part2;
    public GameObject statue_part3;
    public GameObject statue_part4;

    private GameObject[] all_statue;
    private GameObject[] all_parts;
    private int state;



    // Start is called before the first frame update
    void Start()
    {
        all_statue = new GameObject[] { statue1, statue2, statue3, statue4, statue5 };
        all_parts = new GameObject[] { statue_part1, statue_part2, statue_part3, statue_part4};
        state = 0;

        all_statue[0].SetActive(true);
        for (int i=1; i < 5; i++)
        {
            all_statue[i].SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
            Debug.Log("test");
            state += 1;
            if (state == 5)
            {
                state = 0;
            }
            update_visual_state();
        }
    }

    private void update_visual_state()
    {
        for (int i = 0; i < 5; i++)
        {
            all_statue[i].SetActive(false);
        }

        all_statue[state].SetActive(true);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (state<4 && other.gameObject == all_parts[state])
        {
            state += 1;
            update_visual_state();

            //variante de destroy
            if (other.gameObject.GetComponent<OVRGrabbable>().grabbedBy != null)
            {
                other.gameObject.GetComponent<OVRGrabbable>().grabbedBy.ForceRelease(other.gameObject.GetComponent<OVRGrabbable>());
            }
            other.gameObject.SetActive(false);

            //Destroy(other.gameObject);
        }
    }
}
