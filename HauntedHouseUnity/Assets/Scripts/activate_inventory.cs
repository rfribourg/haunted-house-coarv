using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class activate_inventory : MonoBehaviour
{
    public GameObject panel;
    public GameObject locomotion_ctrl;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (OVRInput.GetDown(OVRInput.Button.One))
        {
            panel.SetActive(!panel.activeSelf);
            locomotion_ctrl.SetActive(!panel.activeSelf);

            locomotion_ctrl.transform.parent.GetComponent<SimpleCapsuleWithStickMovement>().enabled = !panel.activeSelf;

        }
    }
}
