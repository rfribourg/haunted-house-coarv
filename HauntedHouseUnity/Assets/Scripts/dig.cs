using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class dig : MonoBehaviour
{
    public AudioClip sound;

    [Range(0f, 1f)]
    public float volume;

    [Range(0.1f, 2.5f)]
    public float pitch;

    public GameObject statue_part;

    private AudioSource source;

    private bool digged; // bool so that the hole can only be digged once

    // Start is called before the first frame update
    void Start()
    {
        this.GetComponent<MeshRenderer>().enabled = false;

        gameObject.AddComponent<AudioSource>(); // The object the sound comes from
        source = GetComponent<AudioSource>();

        source.clip = sound;
        source.volume = volume;
        source.pitch = pitch;

        digged = false;
    }

// Update is called once per frame
    void Update()
    {
        source.volume = volume;
        source.pitch = pitch;
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "DiggingTool" && !digged) // When the shovel is in collision with the hole
        {
            this.GetComponent<MeshRenderer>().enabled = true; // make the pile of dirt visible
            PlaySound(); // play a digging sound
            statue_part.SetActive(true);//make the statue part appear
            digged = true;
        }
    }

    public void PlaySound()
    {
        source.Play();
    }
}
