using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class inventaireScript : MonoBehaviour
{
    public List<buttonScript> listeBoutonScript = new List<buttonScript>();
    public GameObject Button_prefab;
    public List<GameObject> listeBoutonGO= new List<GameObject>();
    public int nombre_boutons;
    public int selected;
    public GameObject player_hand_R;
    public GameObject player_hand_L;
    public GameObject trash;


    // Start is called before the first frame update
    void Start()
    {
        nombre_boutons = 0;

        selected = 0;
    }

    // Update is called once per frame
    void Update()
    {
        int temp;
        if (OVRInput.GetDown(OVRInput.Button.Three))
        {
            if (player_hand_R.GetComponent<OVRGrabber>().grabbedObject != null)
            {
                Add();
            }
            else if (player_hand_L.GetComponent<OVRGrabber>().grabbedObject != null)
            {
                Add();
            }
            else
            {
                Delete();
            }
        }
        if (OVRInput.GetDown(OVRInput.Button.SecondaryIndexTrigger))
        {
            temp = selected +1;
            Debug.Log("Keyup"+temp+selected);
            if (temp < nombre_boutons)
            {
                selected = temp;
            }
            else
            {
                selected = 0;
            }
            changeButton();
        }
    }

    void changeButton()
    {
        if (nombre_boutons > 0)
        {
            listeBoutonScript[selected].Select();
            Debug.Log("Change" + selected);
        }

    }


    IEnumerator Stay(GameObject go)
    {
        go.GetComponent<Rigidbody>().isKinematic = true;
        yield return new WaitForSeconds(1.0f);
        go.GetComponent<Rigidbody>().isKinematic = false;
    }



    void Delete()
    {
        if (nombre_boutons > 0)
        {
         nombre_boutons -= 1;
        }
        GameObject newobj = listeBoutonScript[selected].associatedObject;

        //Ts = tracking space, pour la position
        Transform ts = player_hand_R.transform.parent.parent;

        //On utilise � la fois l'orientation de ma cam�ra et du player car quand on tourne de 45� avec la man�te, cela ne se refl�te pas dans l'orientation de la cam�ra directement
        //On a toujours besoin de la cam�ra pour les angles x/y
        Debug.LogWarning(player_hand_R.transform.parent.parent.GetChild(1).name);
        Transform cam = player_hand_R.transform.parent.parent;//.GetChild(1).GetComponent<Camera>().transform;
        Transform player = player_hand_R.transform.parent.parent.parent.parent;

        //On utilise l'objet pour temporairement stocker l'orientation afin de pouvoir r�cup�rer le forward
        //newobj.transform.eulerAngles = new Vector3(cam.eulerAngles.x, player.eulerAngles.y, cam.eulerAngles.z);
        Vector3 fw = cam.transform.forward;
        fw.y = 0;

        Vector3 obj_center = newobj.transform.InverseTransformPoint(newobj.GetComponent<Renderer>().bounds.center);

        Debug.LogWarning(obj_center);


        //On prend la position du tracking space qui est fiable (celle de la cam�ra ne l'est pas)
        //newobj.transform.position = player.position+player.forward+3*Vector3.up;// + 0.5f*fw +0.5f*Vector3.up;
        newobj.transform.position = transform.position;
        
        //On reset l'orientation qu'on avait juste utilis� temporairement
        //newobj.transform.eulerAngles = Vector3.zero;
        
        //On rend l'objet actif � nouveau
        newobj.SetActive(true);

        //On immobilise l'objet pendant 2 secondes
        StartCoroutine(Stay(newobj));

        //on d�truit les boutons associ�s � l'objet
        Destroy(listeBoutonGO[selected]);
        Destroy(listeBoutonScript[selected]);
        listeBoutonGO.RemoveAt(selected);
        listeBoutonScript.RemoveAt(selected);

        //On change les boutons suivant de place
        for (int i = selected; i < nombre_boutons; i++)
        {
            listeBoutonGO[i].transform.SetParent(transform.GetChild(i + 1), false);
        }
        if (selected >= nombre_boutons && selected!=0)
        {
            selected -= 1;
        }

        Debug.LogWarning("test");
        changeButton();
    }

    void Add()
    {
        nombre_boutons += 1;
        GameObject newobj = new GameObject();
        if (player_hand_R.GetComponent<OVRGrabber>().grabbedObject != null)
        {
            newobj = player_hand_R.GetComponent<OVRGrabber>().grabbedObject.gameObject;
        }
        else if (player_hand_L.GetComponent<OVRGrabber>().grabbedObject != null)
        {
            newobj = player_hand_L.GetComponent<OVRGrabber>().grabbedObject.gameObject;
        }

        newobj.GetComponent<OVRGrabbable>().grabbedBy.ForceRelease(newobj.GetComponent<OVRGrabbable>());


        listeBoutonGO.Add(Instantiate(Button_prefab));
        listeBoutonGO[nombre_boutons-1].transform.SetParent(transform.GetChild(nombre_boutons), false);
        listeBoutonScript.Add(listeBoutonGO[nombre_boutons-1].GetComponent<buttonScript>());
        listeBoutonScript[nombre_boutons - 1].ChangeObj(newobj);
        listeBoutonScript[nombre_boutons-1].Activate();

        //Utiliser destroy serait plus propre mais a pour soucis qu'on ne peut plus mettre l'objet dans l'inventaire par la suite

        newobj.SetActive(false);

        changeButton();
        
    }
}
